package com.example.zadanie.adapter;

import com.example.zadanie.domain.Weather;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class WeatherApiAdapter implements WeatherAdapter {

    @Override
    public List<Weather> buildWeather(final JsonObject object) {
        final List<Weather> weatherList = new ArrayList<>();
        final JsonObject forecast = object.getAsJsonObject("forecast");
        final JsonArray forecastPerDay = forecast.getAsJsonArray("forecastday");

        for(JsonElement dayForecast : forecastPerDay){
            JsonObject dayForecastObject = dayForecast.getAsJsonObject();
            JsonObject data = dayForecastObject.getAsJsonObject("day");
            Weather weather = new Weather(
                    convertDate(dayForecastObject),
                    findTemperature(data),
                    findMaxTemperature(data),
                    findMinTemperature(data),
                    findHumidity(data),
                    findWind(data)
            );
            weatherList.add(weather);
        }

        return weatherList;
    }

    @Override
    public float findTemperature(JsonObject object) {
        return object.get("avgtemp_c").getAsFloat();
    }

    @Override
    public float findMaxTemperature(JsonObject object) {
        return object.get("maxtemp_c").getAsFloat();
    }

    @Override
    public float findMinTemperature(JsonObject object) {
        return object.get("mintemp_c").getAsFloat();
    }

    @Override
    public float findHumidity(JsonObject object) {
        return object.get("avghumidity").getAsFloat();
    }

    @Override
    public float findWind(JsonObject object) {
        return object.get("maxwind_kph").getAsFloat();
    }

    @Override
    public LocalDate convertDate(JsonObject object) {
        String date = object.get("date").getAsString();
        String[] partedDate = date.split("-");

        return LocalDate.of(
                Integer.parseInt(partedDate[0]),
                Integer.parseInt(partedDate[1]),
                Integer.parseInt(partedDate[2])
        );
    }

}
