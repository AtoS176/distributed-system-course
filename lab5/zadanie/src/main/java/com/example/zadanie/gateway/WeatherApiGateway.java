package com.example.zadanie.gateway;

import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class WeatherApiGateway extends Gateway {
    private final static String KEY = "5f04feeb6fdf4df283b194409212405";
    private final static String API = "http://api.weatherapi.com/v1/forecast.json";

    @Override
    protected String createRequest(String city, int days){
        return MessageFormat.format(API + "?key={0}&q={1}&days={2}&aqi=no&alerts=no", KEY, city, days);
    }

}
