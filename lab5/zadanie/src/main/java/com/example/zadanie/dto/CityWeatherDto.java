package com.example.zadanie.dto;

import com.example.zadanie.domain.Weather;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.format.DateTimeFormatter;

public class CityWeatherDto implements Serializable {
    public final String city;
    public final ValueDto maxTemp;
    public final ValueDto minTemp;
    public final ValueDto maxHumidity;
    public final ValueDto minHumidity;
    public final ValueDto maxWind;
    public final ValueDto minWind;
    public final BigDecimal avgTemp;
    public final BigDecimal avgHumidity;
    public final BigDecimal avgWind;

    private CityWeatherDto(Builder builder) {
        this.city = builder.city;
        this.maxTemp = builder.maxTemp;
        this.minTemp = builder.minTemp;
        this.maxHumidity = builder.maxHumidity;
        this.minHumidity = builder.minHumidity;
        this.maxWind = builder.maxWind;
        this.minWind = builder.minWind;
        this.avgTemp = setScale(builder.avgTemp);
        this.avgHumidity = setScale(builder.avgHumidity);
        this.avgWind = setScale(builder.avgWind);
    }

    private BigDecimal setScale(Double value){
        return BigDecimal.valueOf(value).setScale(2, RoundingMode.FLOOR);
    }

    public final static class Builder{
        private final static DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_DATE;
        private String city;
        private ValueDto maxTemp;
        private ValueDto minTemp;
        private ValueDto maxHumidity;
        private ValueDto minHumidity;
        private ValueDto maxWind;
        private ValueDto minWind;
        private Double avgTemp;
        private Double avgHumidity;
        private Double avgWind;

        public Builder withCity(String city){
            this.city = city;
            return this;
        }

        public Builder withMaxTemp(Weather weather){
            this.maxTemp = ValueDto.of(weather.getDate().format(FORMATTER), weather.getMaxTemperature());
            return this;
        }

        public Builder withMinTemp(Weather weather){
            this.minTemp = ValueDto.of(weather.getDate().format(FORMATTER), weather.getMinTemperature());
            return this;
        }

        public Builder withMaxHumidity(Weather weather){
            this.maxHumidity = ValueDto.of(weather.getDate().format(FORMATTER), weather.getHumidity());
            return this;
        }

        public Builder withMinHumidity(Weather weather){
            this.minHumidity = ValueDto.of(weather.getDate().format(FORMATTER), weather.getHumidity());
            return this;
        }

        public Builder withMaxWind(Weather weather){
            this.maxWind = ValueDto.of(weather.getDate().format(FORMATTER), weather.getWind());
            return this;
        }

        public Builder withMinWind(Weather weather){
            this.minWind = ValueDto.of(weather.getDate().format(FORMATTER), weather.getWind());
            return this;
        }

        public Builder withAvgTemp(Double temp){
            this.avgTemp = temp;
            return this;
        }

        public Builder withAvgWind(Double wind){
            this.avgWind = wind;
            return this;
        }

        public Builder withAvgHumidity(Double humidity){
            this.avgHumidity = humidity;
            return this;
        }

        public CityWeatherDto build(){
            return new CityWeatherDto(this);
        }

    }

}
