package com.example.zadanie.gateway;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public abstract class Gateway {
    private final static Logger LOGGER = LoggerFactory.getLogger(Gateway.class);

    public JsonObject getRequestContent(String city, int days) throws IOException, IllegalArgumentException {
        final String web = createRequest(city, days);
        LOGGER.info("API url : " + web);

        final URL url = new URL(web);
        final HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();

        if(request.getResponseCode() >= 400){
            LOGGER.info("Data : " + city + ", " + days + ". Code : " + request.getResponseCode());
            Scanner scanner = new Scanner(request.getErrorStream());

            StringBuilder message = new StringBuilder();

            while(scanner.hasNext()){
                message.append(scanner.next());
                message.append(" ");
            }

            LOGGER.info(message.toString());
            throw new IllegalArgumentException(message.toString());
        }

        final JsonElement jsonElement = JsonParser.parseReader(new InputStreamReader((InputStream) request.getContent()));
        return jsonElement.getAsJsonObject();
    }

    protected abstract String createRequest(String city, int days);
}
