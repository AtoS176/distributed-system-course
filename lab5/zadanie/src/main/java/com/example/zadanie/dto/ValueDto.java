package com.example.zadanie.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class ValueDto implements Serializable {
    public final String date;
    public final BigDecimal value;

    private ValueDto(String date, BigDecimal value) {
        this.date = date;
        this.value = value.setScale(2, RoundingMode.FLOOR);
    }

    public static ValueDto of(String date, Float value){
        return new ValueDto(date, BigDecimal.valueOf(value));
    }
}
