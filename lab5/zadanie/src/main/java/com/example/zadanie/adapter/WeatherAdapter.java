package com.example.zadanie.adapter;

import com.example.zadanie.domain.Weather;
import com.google.gson.JsonObject;

import java.time.LocalDate;
import java.util.List;

public interface WeatherAdapter {
    List<Weather> buildWeather(JsonObject object);
    float findTemperature(JsonObject object);
    float findMaxTemperature(JsonObject object);
    float findMinTemperature(JsonObject object);
    float findHumidity(JsonObject object);
    float findWind(JsonObject object);
    LocalDate convertDate(JsonObject object);
}
