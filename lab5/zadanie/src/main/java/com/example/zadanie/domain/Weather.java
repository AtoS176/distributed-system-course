package com.example.zadanie.domain;

import java.time.LocalDate;

public class Weather {
    private final LocalDate date;
    private final float temperature;
    private final float maxTemperature;
    private final float minTemperature;
    private final float humidity;
    private final float wind;

    public Weather(LocalDate date, float temperature, float maxTemperature, float minTemperature, float humidity, float wind) {
        this.date = date;
        this.temperature = temperature;
        this.maxTemperature = maxTemperature;
        this.minTemperature = minTemperature;
        this.humidity = humidity;
        this.wind = wind;
    }

    public LocalDate getDate() {
        return date;
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getWind() {
        return wind;
    }

    public float getMaxTemperature() {
        return maxTemperature;
    }

    public float getMinTemperature() {
        return minTemperature;
    }
}
