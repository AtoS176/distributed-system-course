package com.example.zadanie.domain;

import com.example.zadanie.dto.CityWeatherDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.ToDoubleFunction;

public final class CityWeather {
    private final static Logger LOGGER = LoggerFactory.getLogger(CityWeather.class);
    private final String city;
    private final List<Weather> weatherPerDay;

    public CityWeather(String city, List<Weather> weatherPerDay) {
        this.city = city;
        this.weatherPerDay = weatherPerDay;
    }

    public Double calculateAvgValue(ToDoubleFunction<Weather> calculateMethod) {
        return weatherPerDay
                .stream()
                .mapToDouble(calculateMethod)
                .average()
                .orElseThrow(() -> {
                    LOGGER.info(weatherPerDay.toString());
                    return new NoSuchElementException();
                });
    }

    public Weather findMax(Comparator<Weather> comparator) {
        return weatherPerDay
                .stream()
                .max(comparator)
                .orElseThrow(() -> {
                    LOGGER.info(weatherPerDay.toString());
                    return new NoSuchElementException();
                });
    }

    public Weather findMin(Comparator<Weather> comparator) {
        return weatherPerDay
                .stream()
                .min(comparator)
                .orElseThrow(() -> {
                    LOGGER.info(weatherPerDay.toString());
                    return new NoSuchElementException();
                });
    }

    public CityWeatherDto makeAnalyse(){
        return new CityWeatherDto.Builder()
                .withCity(city)
                //calculate and add average parameters
                .withAvgHumidity(calculateAvgValue(Weather::getHumidity))
                .withAvgTemp(calculateAvgValue(Weather::getTemperature))
                .withAvgWind(calculateAvgValue(Weather::getWind))
                //calculate and add max value of parameters
                .withMaxHumidity(findMax(Comparator.comparing(Weather::getHumidity)))
                .withMaxTemp(findMax(Comparator.comparing(Weather::getMaxTemperature)))
                .withMaxWind(findMax(Comparator.comparing(Weather::getWind)))
                //calculate and add min value of parameters
                .withMinHumidity(findMin(Comparator.comparing(Weather::getHumidity)))
                .withMinTemp(findMin(Comparator.comparing(Weather::getMinTemperature)))
                .withMinWind(findMin(Comparator.comparing(Weather::getWind)))
                //create DTO
                .build();
    }

}
