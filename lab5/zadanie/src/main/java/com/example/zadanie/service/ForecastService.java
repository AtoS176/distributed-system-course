package com.example.zadanie.service;

import com.example.zadanie.adapter.WeatherApiAdapter;
import com.example.zadanie.domain.CityWeather;
import com.example.zadanie.domain.Weather;
import com.example.zadanie.dto.CityWeatherDto;
import com.example.zadanie.gateway.WeatherApiGateway;
import com.google.gson.JsonObject;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ForecastService {
    private final WeatherApiGateway apiGateway;
    private final WeatherApiAdapter apiAdapter;

    public ForecastService(WeatherApiGateway apiGateway, WeatherApiAdapter apiAdapter) {
        this.apiGateway = apiGateway;
        this.apiAdapter = apiAdapter;
    }

    public Either<String, CityWeatherDto> createForecast(String city, int days){
        if(days <= 0){
            return Either.left("Liczba dni musi być większa niż 0.");
        }

        if(days > 3){
            return Either.left("Darmowe API umożliwia zobaczenie pogody na 3 dni do przodu.");
        }

        Try<JsonObject> dataFromAPI = Try.of(() -> apiGateway.getRequestContent(city, days));

        if(dataFromAPI.isFailure()){
            return Either.left(dataFromAPI.getCause().getMessage());
        }

        List<Weather> weatherList = apiAdapter.buildWeather(dataFromAPI.get());
        CityWeather cityWeather = new CityWeather(city, weatherList);

        return Either.right(cityWeather.makeAnalyse());
    }

}
