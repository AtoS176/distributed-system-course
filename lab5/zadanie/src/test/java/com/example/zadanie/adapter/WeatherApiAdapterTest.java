package com.example.zadanie.adapter;

import com.example.zadanie.gateway.WeatherApiGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WeatherApiAdapterTest {
    private final WeatherAdapter weatherAdapter = new WeatherApiAdapter();
    private final WeatherApiGateway weatherApiGateway = new WeatherApiGateway();

    @Test
    public void when() throws Exception {
        //given
        String city = "London";
        int days = 2;

        //when
        weatherAdapter.buildWeather(weatherApiGateway.getRequestContent(city, days));

        //then
        Assertions.assertTrue(true);
    }

}
