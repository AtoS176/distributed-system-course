package client;

import io.vavr.control.Try;
import server.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;

public class ClientWriter implements Runnable {
    private final PrintWriter writer;
    private final DatagramSocket datagramSocket;

    public ClientWriter(PrintWriter writer, DatagramSocket datagramSocket) {
        this.writer = writer;
        this.datagramSocket = datagramSocket;
    }

    @Override
    public void run() {
        //only to run UDP and save correct port in map
        Try.run(() -> sendByUdp(""));

        while(true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            try {
                String message = br.readLine();
                if (message != null) {
                    if (!trySendByUDP(message)) sendByTcp(message);
                    if (message.trim().equals("BYE")) Client.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendByTcp(String message){
        writer.println(message);
    }

    private boolean trySendByUDP(String message) throws IOException {
        final String udpPattern = "-U";
        final String multicastPattern = "-M";

        if(message.contains(udpPattern)){
            sendByUdp(message);
            return true;
        }

        if(message.contains(multicastPattern)){
            sendByMulticast(message);
            return true;
        }

        return false;
    }

    private void sendByUdp(String message) throws IOException {
        InetAddress address = InetAddress.getByName(Client.HOSTNAME);
        byte[] sendBuffer = message.replace("-U", "").getBytes(StandardCharsets.UTF_8);

        DatagramPacket sendPacket = new DatagramPacket(
                sendBuffer,
                sendBuffer.length,
                address,
                Server.SERVER_PORT
        );

        datagramSocket.send(sendPacket);
    }

    private void sendByMulticast(String message) throws IOException {
        InetAddress address = InetAddress.getByName(Client.MULTICAST);
        byte[] sendBuffer = message.replace("-M", "").getBytes(StandardCharsets.UTF_8);

        //packet to send by UDP
        DatagramPacket sendPacket = new DatagramPacket(
                sendBuffer,
                sendBuffer.length,
                address,
                Client.MULTICAST_PORT
        );

        datagramSocket.send(sendPacket);
    }

}
