package client;

import io.vavr.control.Try;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;

public class ClientUdpReader implements Runnable {
    private final DatagramSocket datagramSocket;
    private final String connectionType;

    public ClientUdpReader(String connectionType, DatagramSocket datagramSocket) {
        this.connectionType = connectionType;
        this.datagramSocket = datagramSocket;
    }

    @Override
    public void run() {
        byte[] receiveBuffer = new byte[1024];

        while (true) {
            Arrays.fill(receiveBuffer, (byte) 0);
            DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
            Try<Void> tryReceive = Try.run(() -> datagramSocket.receive(receivePacket));

            if (tryReceive.isSuccess()) {
                String msg = new String(receivePacket.getData());
                if (!msg.trim().isEmpty()) {
                    System.out.println(connectionType + " " + msg);
                }
            }
        }
    }

}
