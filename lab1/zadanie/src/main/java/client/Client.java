package client;

import server.Server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;

public class Client {
    public static String HOSTNAME = "localhost";
    public static String MULTICAST = "225.128.0.0";
    public static int MULTICAST_PORT = 4444;

    public static void main(String[] args) {
        System.out.println("JAVA CLIENT");

        try (Socket socket = new Socket(HOSTNAME, Server.SERVER_PORT);
             DatagramSocket datagramSocket = new DatagramSocket();
             MulticastSocket multicastSocket = new MulticastSocket(MULTICAST_PORT)) {

            // bind multicast
            InetAddress multicast = InetAddress.getByName(MULTICAST);
            multicastSocket.joinGroup(multicast);

            // TCP : in & out streams
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            // TCP&UDP send msg
            Runnable writer = new ClientWriter(out, datagramSocket);
            CompletableFuture<Void> writeTask = CompletableFuture.runAsync(writer);

            // read  TCP msg
            ClientTcpReader reader = new ClientTcpReader(in);
            CompletableFuture.runAsync(reader);

            // read  UDP msg
            final String udpType = "UDP";
            ClientUdpReader udpReader = new ClientUdpReader(udpType, datagramSocket);
            CompletableFuture.runAsync(udpReader);

            // read Multicast
            final String multicastType = "MULTICAST";
            ClientUdpReader multicastReader = new ClientUdpReader(multicastType, multicastSocket);
            CompletableFuture.runAsync(multicastReader);

            //block writeTask thread : exit when BYE
            writeTask.get();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void close(){
        System.exit(0);
    }

}