package client;

import io.vavr.control.Try;

import java.io.BufferedReader;

public class ClientTcpReader implements Runnable {
    private final BufferedReader reader;

    public ClientTcpReader(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public void run() {
        while (true){
            Try<String> response = Try.of(reader::readLine);
            if(response.isSuccess() && response.get()!= null){
                System.out.println("TCP -> " + response.get());
            }
        }
    }

}
