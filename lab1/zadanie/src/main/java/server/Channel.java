package server;

import java.io.BufferedReader;
import java.io.IOException;

public class Channel implements Runnable {
    private final Server server;
    private final BufferedReader reader;
    private final int clientId;

    public Channel(Server server, BufferedReader reader, int clientId) {
        this.server = server;
        this.reader = reader;
        this.clientId = clientId;
    }

    @Override
    public void run() {
        boolean done = false;
        while(!done){
            try {
                String msg = reader.readLine();
                if(msg != null && !msg.trim().isEmpty()){
                    server.sendToAll(clientId, msg);
                    if(msg.trim().equals("BYE")) done = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
