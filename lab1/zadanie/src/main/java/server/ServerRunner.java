package server;

import java.util.concurrent.CompletableFuture;

public class ServerRunner {
    public static void main(String[] args) {
        Server server = new Server();
        CompletableFuture.runAsync(server::run);
        UdpServer udpServer = new UdpServer();
        udpServer.run();
    }
}
