package server;

import client.Client;
import io.vavr.control.Try;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpServer {
    private final static Logger LOGGER = Logger.getLogger(UdpServer.class.getName());
    public final static String WELCOME_MESSAGE = "UDP server is active";
    public final static int SERVER_PORT = 8189;

    private final Map<Integer, Integer> portsMap = new HashMap<>();
    private int idCounter = 0;

    public void run() {
        LOGGER.log(Level.INFO, WELCOME_MESSAGE);
        try (var datagramSocket = new DatagramSocket(SERVER_PORT)) {
            while (true) {
                byte[] receiveBuffer = new byte[1024];
                Arrays.fill(receiveBuffer, (byte) 0);

                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                Try<Void> receivedData = Try.run(() -> datagramSocket.receive(receivePacket));

                if (receivedData.isSuccess()) {
                    handleReceivedData(receivePacket, datagramSocket);
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private void incrementId() {
        idCounter += 1;
    }

    public void handleReceivedData(DatagramPacket receivePacket, DatagramSocket socket) {
        String msg = new String(receivePacket.getData());

        if (msg.trim().isEmpty() && !portsMap.containsKey(receivePacket.getPort())) {
            incrementId();
            portsMap.put(receivePacket.getPort(), idCounter);
        } else {
            sendToAll(receivePacket.getPort(), msg, socket);
        }
    }

    private void sendToAll(int clientPort, String message, DatagramSocket socket) {
        final int clientId = portsMap.get(clientPort);
        final String answer = "From client " + clientId + " : " + message;
        LOGGER.log(Level.INFO, "UDP -> Send From client : " + clientId);

        if (message.equals("BYE")) {
            portsMap.remove(clientPort);
            LOGGER.log(Level.INFO, "Client " + clientId + " was removed.");
        }

        portsMap.forEach((port, id) -> {
            if (!port.equals(clientPort)) {
                Try.run(() -> sendMessage(answer, socket, port));
            }
        });
    }

    private void sendMessage(String message, DatagramSocket datagramSocket, int port) throws IOException {
        InetAddress address = InetAddress.getByName(Client.HOSTNAME);
        byte[] sendBuffer = message.getBytes(StandardCharsets.UTF_8);
        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, address, port);
        datagramSocket.send(sendPacket);
    }

}
