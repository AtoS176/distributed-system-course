package server;

import io.vavr.control.Try;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {
    private final static Logger LOGGER = Logger.getLogger(Server.class.getName());
    public final static String WELCOME_MESSAGE = "TCP server is active";
    public final static int SERVER_PORT = 8189;

    private final Map<Integer, PrintWriter> clientMap = new HashMap<>();
    private final AtomicInteger idCounter = new AtomicInteger(0);

    public void run(){
        LOGGER.log(Level.INFO, WELCOME_MESSAGE);
        try(var socket = new ServerSocket(SERVER_PORT)){
            while(true) {
                incrementId();
                Socket client = socket.accept();

                //insert client output to map
                PrintWriter out = new PrintWriter(client.getOutputStream(), true);
                out.println("Your id is : " + idCounter.get());
                clientMap.put(idCounter.get(), out);

                //run tcp communication channel on tcp service pool
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                Runnable channel = new Channel(this, in, idCounter.get());
                Thread thread = new Thread(channel);
                thread.start();
            }
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    public synchronized void sendToAll(int clientId, String message){
        final String answer = "From client " + clientId + " : " + message;
        LOGGER.log(Level.INFO, "UDP -> Send From client : " + clientId);

        //delete if socket close
        if(message.equals("BYE")){
            clientMap.remove(clientId);
            LOGGER.log(Level.INFO, "Client " + clientId + " was removed.");
        }

        clientMap.forEach((id, writer) -> {
            if(!id.equals(clientId)){
                Try.run(() -> writer.println(answer));
            }
        });
    }

    public void incrementId(){
        idCounter.incrementAndGet();
    }

}
