import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class JavaUdpServer {
    private final static Logger LOGGER = Logger.getLogger(JavaUdpServer.class.getName());

    public static void main(String args[])
    {
        System.out.println("JAVA UDP SERVER");

        int portNumber = 9008;
        try (DatagramSocket socket = new DatagramSocket(portNumber)) {
            byte[] receiveBuffer = new byte[1024];

            while (true) {
                Arrays.fill(receiveBuffer, (byte) 0);
                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                socket.receive(receivePacket);

                final int receivedInt = bytesToInt(receivePacket.getData());
                System.out.println("received msg: " + receivedInt);

                //Odesłanie odpowiedzi do klienta
                final byte[] answer = intToBytes(receivedInt+1);
                final InetAddress clientAddress = receivePacket.getAddress();
                final int clientPort = receivePacket.getPort();

                LOGGER.log(Level.INFO, "Client Address : " + clientAddress);
                LOGGER.log(Level.INFO, "Client Port : " + clientPort);

                final DatagramPacket answerDatagram = new DatagramPacket(answer, answer.length, clientAddress, clientPort);
                socket.send(answerDatagram);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Funkcja do konwersji tablicy bajtów na wartośc typu int.
     * Korzystamy w kolejności Little_Endian, ponieważ w takiej kolejności
     * są przesyłane bajty od klienta UDP Python
     *
     * @param bytes tablica bajtów otrzymana od klienta UDP python
     *
     * @return wartosc int
     */
    public static int bytesToInt(byte[] bytes){
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    /**
     * Funkcja która konweruja inta na tablicę bajtów.
     * Uzywamy kolejnosci Little_Endian - takiej samej jak klient UDP Python.
     *
     * @param number wartosc liczbowa ktora ma zostac wyslana do klienta
     * @return tablica bajtów gotowa do przeslania
     */
    public static byte[] intToBytes(final int number){
        final int size = 4;
        return ByteBuffer.allocate(size).order(ByteOrder.LITTLE_ENDIAN).putInt(number).array();
    }

}
