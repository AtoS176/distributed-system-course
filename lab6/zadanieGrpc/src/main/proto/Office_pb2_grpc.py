# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import Office_pb2 as Office__pb2


class BirthCertificateStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.PrepareBirthCertificate = channel.unary_unary(
                '/gen.BirthCertificate/PrepareBirthCertificate',
                request_serializer=Office__pb2.BirthCertificateRequest.SerializeToString,
                response_deserializer=Office__pb2.BasicResponse.FromString,
                )
        self.BirthCertificateResult = channel.unary_unary(
                '/gen.BirthCertificate/BirthCertificateResult',
                request_serializer=Office__pb2.ClientId.SerializeToString,
                response_deserializer=Office__pb2.BirthCertificateResponse.FromString,
                )


class BirthCertificateServicer(object):
    """Missing associated documentation comment in .proto file."""

    def PrepareBirthCertificate(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def BirthCertificateResult(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_BirthCertificateServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'PrepareBirthCertificate': grpc.unary_unary_rpc_method_handler(
                    servicer.PrepareBirthCertificate,
                    request_deserializer=Office__pb2.BirthCertificateRequest.FromString,
                    response_serializer=Office__pb2.BasicResponse.SerializeToString,
            ),
            'BirthCertificateResult': grpc.unary_unary_rpc_method_handler(
                    servicer.BirthCertificateResult,
                    request_deserializer=Office__pb2.ClientId.FromString,
                    response_serializer=Office__pb2.BirthCertificateResponse.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'gen.BirthCertificate', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class BirthCertificate(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def PrepareBirthCertificate(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/gen.BirthCertificate/PrepareBirthCertificate',
            Office__pb2.BirthCertificateRequest.SerializeToString,
            Office__pb2.BasicResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def BirthCertificateResult(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/gen.BirthCertificate/BirthCertificateResult',
            Office__pb2.ClientId.SerializeToString,
            Office__pb2.BirthCertificateResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)


class CutTreeStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.AllowToCutTree = channel.unary_unary(
                '/gen.CutTree/AllowToCutTree',
                request_serializer=Office__pb2.CutTreeRequest.SerializeToString,
                response_deserializer=Office__pb2.BasicResponse.FromString,
                )
        self.CutTreeResult = channel.unary_unary(
                '/gen.CutTree/CutTreeResult',
                request_serializer=Office__pb2.ClientId.SerializeToString,
                response_deserializer=Office__pb2.CutTreeResponse.FromString,
                )


class CutTreeServicer(object):
    """Missing associated documentation comment in .proto file."""

    def AllowToCutTree(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CutTreeResult(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_CutTreeServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'AllowToCutTree': grpc.unary_unary_rpc_method_handler(
                    servicer.AllowToCutTree,
                    request_deserializer=Office__pb2.CutTreeRequest.FromString,
                    response_serializer=Office__pb2.BasicResponse.SerializeToString,
            ),
            'CutTreeResult': grpc.unary_unary_rpc_method_handler(
                    servicer.CutTreeResult,
                    request_deserializer=Office__pb2.ClientId.FromString,
                    response_serializer=Office__pb2.CutTreeResponse.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'gen.CutTree', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class CutTree(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def AllowToCutTree(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/gen.CutTree/AllowToCutTree',
            Office__pb2.CutTreeRequest.SerializeToString,
            Office__pb2.BasicResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CutTreeResult(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/gen.CutTree/CutTreeResult',
            Office__pb2.ClientId.SerializeToString,
            Office__pb2.CutTreeResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)


class CarRegisterStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.ReportCarRegister = channel.unary_unary(
                '/gen.CarRegister/ReportCarRegister',
                request_serializer=Office__pb2.CarRegisterRequest.SerializeToString,
                response_deserializer=Office__pb2.BasicResponse.FromString,
                )
        self.CarRegisterResult = channel.unary_unary(
                '/gen.CarRegister/CarRegisterResult',
                request_serializer=Office__pb2.ClientId.SerializeToString,
                response_deserializer=Office__pb2.CarRegisterResponse.FromString,
                )


class CarRegisterServicer(object):
    """Missing associated documentation comment in .proto file."""

    def ReportCarRegister(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def CarRegisterResult(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_CarRegisterServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'ReportCarRegister': grpc.unary_unary_rpc_method_handler(
                    servicer.ReportCarRegister,
                    request_deserializer=Office__pb2.CarRegisterRequest.FromString,
                    response_serializer=Office__pb2.BasicResponse.SerializeToString,
            ),
            'CarRegisterResult': grpc.unary_unary_rpc_method_handler(
                    servicer.CarRegisterResult,
                    request_deserializer=Office__pb2.ClientId.FromString,
                    response_serializer=Office__pb2.CarRegisterResponse.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'gen.CarRegister', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class CarRegister(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def ReportCarRegister(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/gen.CarRegister/ReportCarRegister',
            Office__pb2.CarRegisterRequest.SerializeToString,
            Office__pb2.BasicResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def CarRegisterResult(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/gen.CarRegister/CarRegisterResult',
            Office__pb2.ClientId.SerializeToString,
            Office__pb2.CarRegisterResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
