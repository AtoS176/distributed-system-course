import grpc

import Office_pb2
import Office_pb2_grpc


def run():
    channel = grpc.insecure_channel('localhost:6790')

    print('Pozwolenie na wycinke drzewa.')
    stub1 = Office_pb2_grpc.CutTreeStub(channel)
    response1 = stub1.AllowToCutTree(Office_pb2.CutTreeRequest(street='Zielona', street_number=25, city='Katowice'))

    print("Please remember your ID: " + response1.id)
    print("Waiting time: " + str(response1.time))
    print()

    print('Wniosek o akt urodzenia.')
    stub2 = Office_pb2_grpc.BirthCertificateStub(channel)
    response2 = stub2.PrepareBirthCertificate(Office_pb2.BirthCertificateRequest(id='99091007795'))

    print("Please remember your ID: " + response2.id)
    print("Waiting time: " + str(response2.time))
    print()

    print('Rejestracja auta.')
    stub3 = Office_pb2_grpc.CarRegisterStub(channel)
    response3 = stub3.ReportCarRegister(Office_pb2.CarRegisterRequest(brand='Opel', model='Astra', city='Kraków'))

    print("Please remember your ID: " + response3.id)
    print("Waiting time: " + str(response3.time))
    print()

    while 1:
        print('Enter your ID:')
        x = input()
        print('Your ID : ' + x)

        if x.__contains__('A'):
            try:
                response2 = stub2.BirthCertificateResult(Office_pb2.ClientId(id=x))
                print("Name: " + response2.name)
                print("Surname: " + response2.surname)
                print("Birthday: " + response2.birthdate)
                print("Birth City: " + response2.birth_city)
                print("Father: " + response2.father_full_name)
                print("Mother: " + response2.mother_full_name)
            except grpc.RpcError as rpc_error:
                print("Info: " + rpc_error.details())
        if x.__contains__('B'):
            try:
                response1 = stub1.CutTreeResult(Office_pb2.ClientId(id=x))
                print("Permission: " + str(response1.permission))
            except grpc.RpcError as rpc_error:
                print("Info: " + rpc_error.details())
        if x.__contains__('C'):
            try:
                response1 = stub3.CarRegisterResult(Office_pb2.ClientId(id=x))
                print("Car ID: " + str(response1.identification))
            except grpc.RpcError as rpc_error:
                print("Info: " + rpc_error.details())


run()
