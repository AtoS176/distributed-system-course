package office;

import gen.*;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

public class CutTreeDepartment  extends CutTreeGrpc.CutTreeImplBase {
    private final ClosedApplications<CutTreeResponse> map = new ClosedApplications<>();

    private void consume(int time, String id){
        try {
            Thread.sleep(time * 1000L);
            CutTreeResponse cutTreeResponse = CutTreeResponse.newBuilder().setPermission(new Random().nextBoolean()).build();
            map.add(cutTreeResponse, id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void allowToCutTree(CutTreeRequest request, StreamObserver<BasicResponse> responseObserver) {
        int time = map.getTime();
        String id = map.getId("B");

        CompletableFuture.runAsync(() -> consume(time, id));

        BasicResponse basicResponse = BasicResponse.newBuilder()
                .setId(id)
                .setTime(time)
                .build();

        responseObserver.onNext(basicResponse);
        responseObserver.onCompleted();
    }


    @Override
    public void cutTreeResult(ClientId request, StreamObserver<CutTreeResponse> responseObserver) {
        if(!map.containsKey(request.getId())){
            responseObserver.onError(
                    Status.INVALID_ARGUMENT
                    .withDescription("Application is being processed")
                    .asRuntimeException()
            );
            return;
        }

        responseObserver.onNext(map.get(request.getId()));
        responseObserver.onCompleted();
    }

}
