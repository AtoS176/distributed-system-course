package office;

import gen.*;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

import java.util.Random;
import java.util.concurrent.CompletableFuture;

public class CarRegisterDepartment extends CarRegisterGrpc.CarRegisterImplBase {
    private final ClosedApplications<CarRegisterResponse> map = new ClosedApplications<>();

    private void consume(int time, String id, String city){
        try {
            Thread.sleep(time * 1000L);
            CarRegisterResponse carRegisterResponse = CarRegisterResponse
                    .newBuilder()
                    .setIdentification(city.substring(0, 3) +  (new Random().nextInt(8000) + 1000))
                    .build();

            map.add(carRegisterResponse, id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reportCarRegister(CarRegisterRequest request, StreamObserver<BasicResponse> responseObserver) {
        int time = map.getTime();
        String id = map.getId("C");

        CompletableFuture.runAsync(() -> consume(time, id, request.getCity()));

        BasicResponse basicResponse = BasicResponse.newBuilder()
                .setId(id)
                .setTime(time)
                .build();

        responseObserver.onNext(basicResponse);
        responseObserver.onCompleted();
    }

    @Override
    public void carRegisterResult(ClientId request, StreamObserver<CarRegisterResponse> responseObserver) {
        if(!map.containsKey(request.getId())){
            responseObserver.onError(
                    Status.INVALID_ARGUMENT
                            .withDescription("Application is being processed")
                            .asRuntimeException()
            );
            return;
        }

        responseObserver.onNext(map.get(request.getId()));
        responseObserver.onCompleted();
    }
}
