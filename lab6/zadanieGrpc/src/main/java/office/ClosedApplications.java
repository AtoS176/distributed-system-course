package office;

import java.util.HashMap;
import java.util.Random;

public class ClosedApplications<T> {
    private final HashMap<String, T> map = new HashMap<>();
    private int counter = 1;

    public int getTime(){
        return new Random().nextInt(15) + 10;
    }

    public String getId(String type){
        return type + counter++;
    }

    public void add(T object, String key){
        map.put(key, object);
    }

    public boolean containsKey(String key){
        return map.containsKey(key);
    }

    public T get(String key){
        return map.get(key);
    }

}
