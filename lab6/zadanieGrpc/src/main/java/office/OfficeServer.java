package office;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class OfficeServer {
    private static final Logger LOGGER = Logger.getLogger(OfficeServer.class.getName());

    private Server server;

    private void start() throws IOException {
        int port = 6790;
        server = ServerBuilder.forPort(port)
                .addService(new CutTreeDepartment())
                .addService(new BirthCertificateDepartment())
                .addService(new CarRegisterDepartment())
                .build()
                .start();

        LOGGER.info("Server started, listening on " + port);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            try {
                OfficeServer.this.stop();
            } catch (InterruptedException e) {
                e.printStackTrace(System.err);
            }
            System.err.println("*** server shut down");
        }));
    }

    private void stop() throws InterruptedException {
        if (server != null) {
            server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        final OfficeServer server = new OfficeServer();
        server.start();
        server.blockUntilShutdown();
    }

}

