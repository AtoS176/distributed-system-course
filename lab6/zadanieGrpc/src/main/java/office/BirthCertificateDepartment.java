package office;

import gen.*;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

import java.text.MessageFormat;
import java.util.List;
import java.util.Random;

public class BirthCertificateDepartment extends BirthCertificateGrpc.BirthCertificateImplBase {
    private final ClosedApplications<BirthCertificateResponse> map = new ClosedApplications<>();

    private final List<String> manNames = List.of("Tomek", "Krzysiek", "Jan", "Franek");
    private final List<String> womanNames = List.of("Ala", "Ola", "Zosia", "Ela");
    private final List<String> surnames = List.of("Nowak", "Kowal", "Kot", "Lis");
    private final List<String> cities = List.of("Katowice", "Lublin", "Warszawa", "Szczecin");

    private String prepareBirthDate(String id) {
        int year = 0;
        String  monthStr = id.substring(2, 4);

        int month = 0;

        if (monthStr.charAt(0) != '0') {
            month = Integer.parseInt(monthStr);
        } else {
            month = Integer.parseInt(monthStr.substring(1));
        }

        if (month > 12) {
            month = month - 20;
            year = 2000 + Integer.parseInt(id.substring(0, 2));
        } else {
            year = 1900 + Integer.parseInt(id.substring(0, 2));
        }

        int day = 0;
        String dayAsString = id.substring(4, 6);

        if (dayAsString.charAt(0) != '0') {
            day = Integer.parseInt(dayAsString);
        } else {
            day = Integer.parseInt(dayAsString.substring(1));
        }

        return MessageFormat.format("{0,number,#} - {1} - {2}", year, month, day);
    }

    private String getRandom(List<String> items) {
        return items.get(new Random().nextInt(items.size()));
    }

    private void consume(BirthCertificateRequest request, int time, String id){
        try {
            Thread.sleep(time * 1000L);
            String surname = getRandom(surnames);

            BirthCertificateResponse birthCertificateResponse = BirthCertificateResponse.newBuilder()
                    .setName(getRandom(manNames))
                    .setSurname(getRandom(surnames))
                    .setBirthdate(prepareBirthDate(request.getId()))
                    .setBirthCity(getRandom(cities))
                    .setFatherFullName(getRandom(manNames) + " " + surname)
                    .setMotherFullName(getRandom(womanNames) + " " + surname)
                    .build();

            map.add(birthCertificateResponse, id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void prepareBirthCertificate(BirthCertificateRequest request, StreamObserver<BasicResponse> responseObserver) {
        int time = map.getTime();
        String id = map.getId("A");

        new Thread(() -> consume(request, time, id)).start();

        BasicResponse basicResponse = BasicResponse.newBuilder()
                .setId(id)
                .setTime(time)
                .build();

        responseObserver.onNext(basicResponse);
        responseObserver.onCompleted();
    }

    @Override
    public void birthCertificateResult(ClientId request, StreamObserver<BirthCertificateResponse> responseObserver) {
        if(!map.containsKey(request.getId())){
            responseObserver.onError(
                    Status.INVALID_ARGUMENT
                            .withDescription("Application is being processed")
                            .asRuntimeException()
            );
            return;
        }

        responseObserver.onNext(map.get(request.getId()));
        responseObserver.onCompleted();
    }

}
