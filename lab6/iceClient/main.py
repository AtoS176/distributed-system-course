import sys
import Ice
import Demo


communicator = Ice.initialize()
base = communicator.stringToProxy(sys.argv[1] + '/' + sys.argv[2] + ':tcp -h 127.0.0.55 -p 10000 -z')
adder = Demo.CalcPrx.checkedCast(base)

if not adder:
    print('Invalid proxy')
    sys.exit(1)

while True:
    a = input("Enter a: ")
    b = input("Enter b: ")
    r = adder.add(int(a), int(b))
    print('Result : ' + str(r))
