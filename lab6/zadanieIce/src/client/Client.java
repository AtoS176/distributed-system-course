package client;

import Demo.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.Exception;

import com.zeroc.Ice.*;

public class Client {
    public static void main(String[] args) {
        int status = 0;
        Communicator communicator = null;

        try {
            communicator = Util.initialize(args);
            ObjectPrx base1 = communicator.stringToProxy("shared/calc2:tcp -h 127.0.0.55 -p 10000 -z");

            CalcPrx obj1 = CalcPrx.checkedCast(base1);

            String a = null;
            String b = null;
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            while(true){
                System.out.println("Enter a : ");
                a = in.readLine();
                System.out.println("Enter b : ");
                b = in.readLine();

                try{
                    int aInt = Integer.parseInt(a);
                    int bInt = Integer.parseInt(b);
                    long result = obj1.add(aInt, bInt);
                    System.out.println("Result : " + result);
                }catch (NumberFormatException ex){
                    System.out.println("Incorrect Args : a and b must be a number.");
                }
            }

        } catch (LocalException e) {
            e.printStackTrace();
            status = 1;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            status = 1;
        }

        if (communicator != null) {
            try {
                communicator.destroy();
            } catch (Exception e) {
                System.err.println(e.getMessage());
                status = 1;
            }
        }

        System.exit(status);
    }

}
