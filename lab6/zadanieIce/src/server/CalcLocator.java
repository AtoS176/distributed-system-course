package server;

import com.zeroc.Ice.*;
import com.zeroc.Ice.Object;

import java.util.HashMap;
import java.util.Map;

public class CalcLocator implements ServantLocator {
    private int servantId = 1;
    private final Map<String, CalcImpl> servants = new HashMap<>();

    @Override
    public LocateResult locate(Current current) {
        String objectID = current.id.toString();
        servants.putIfAbsent(objectID, new CalcImpl(servantId++));
        return new LocateResult(servants.get(objectID), null);
    }

    @Override
    public void finished(Current current, Object object, java.lang.Object o) {
    }

    @Override
    public void deactivate(String s) {
    }

}
