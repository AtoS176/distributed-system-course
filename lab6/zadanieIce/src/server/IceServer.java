package server;

import com.zeroc.Ice.Communicator;
import com.zeroc.Ice.Util;
import com.zeroc.Ice.ObjectAdapter;
import com.zeroc.Ice.Identity;

public class IceServer {
    public void launch(String[] args) {
        int status = 0;
        Communicator communicator = null;

        try {
            communicator = Util.initialize(args);

            ObjectAdapter adapter = communicator.createObjectAdapterWithEndpoints("Adapter", "tcp -h 127.0.0.55 -p 10000 -z");

            CalcImpl sharedServant = new CalcImpl(0);

            adapter.add(sharedServant, new Identity("calc1", "shared"));
            adapter.add(sharedServant, new Identity("calc2", "shared"));

            adapter.addServantLocator(new CalcLocator(), "separate");

            adapter.activate();
            communicator.waitForShutdown();

        } catch (Exception e) {
            System.err.println(e.getMessage());
            status = 1;
        }

        if (communicator != null) {
            try {
                communicator.destroy();
            } catch (Exception e) {
                System.err.println(e.getMessage());
                status = 1;
            }
        }

        System.exit(status);
    }


    public static void main(String[] args) {
        IceServer app = new IceServer();
        app.launch(args);
    }

}
