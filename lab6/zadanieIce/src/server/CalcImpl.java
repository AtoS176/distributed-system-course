package server;

import Demo.Calc;
import com.zeroc.Ice.Current;

import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CalcImpl implements Calc {
    private final static Logger LOGGER = Logger.getLogger(CalcImpl.class.getName());
    private final Integer id;

    public CalcImpl(Integer id) {
        this.id = id;
    }

    @Override
    public int add(int a, int b, Current __current) {
        LOGGER.log(Level.INFO, MessageFormat.format(
                "Called add operation ; a = {0} ; b = {1} ; Object : {2}, Servant : {3}",
                a, b,
                __current.id.toString(),
                id
                )
        );
        return a + b;
    }

}
