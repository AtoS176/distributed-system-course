import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

public class ChildrenCounter implements Watcher, Runnable {
    final ZookeeperTree zookeeperTree;
    final ZooKeeper zooKeeper;
    final String child;

    private ChildrenCounter(ZooKeeper zooKeeper, String child, ZookeeperTree zookeeperTree) {
        this.zookeeperTree = zookeeperTree;
        this.zooKeeper = zooKeeper;
        this.child = child;
    }

    public static ChildrenCounter createOf(ZooKeeper zooKeeper, String node, ZookeeperTree zookeeperTree){
        return new ChildrenCounter(zooKeeper, node, zookeeperTree);
    }

    @Override
    public void run() {
        if(zookeeperTree.ifNodeExist(child)){
            try {
                zookeeperTree.subscribeChildren(child, this);
            } catch (KeeperException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void process(WatchedEvent event) {
        try {
            zookeeperTree.subscribeChildren(Main.Z_NODE, this);
            System.out.println("Counter : " + zooKeeper.getAllChildrenNumber(Main.Z_NODE));
        } catch (KeeperException | InterruptedException e) {
            e.printStackTrace();
        }

    }
}
