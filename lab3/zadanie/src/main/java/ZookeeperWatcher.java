import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;

public class ZookeeperWatcher implements Watcher {
    final static String NODE_CREATED = "/z node was created";
    final static String NODE_DELETED = "/z node was deleted";
    final ZookeeperTree zookeeperTree;
    final ZooKeeper zooKeeper;
    private Process process;

    private ZookeeperWatcher(ZooKeeper zooKeeper, ZookeeperTree zookeeperTree) {
        this.zooKeeper = zooKeeper;
        this.zookeeperTree = zookeeperTree;
    }

    public static ZookeeperWatcher createOf(ZooKeeper zooKeeper, ZookeeperTree zookeeperTree){
        return new ZookeeperWatcher(zooKeeper, zookeeperTree);
    }

    @Override
    public void process(WatchedEvent event) {
        if(Event.EventType.NodeCreated.equals(event.getType())){
            if(process == null) {
                System.out.println(NODE_CREATED);
                try {
                    process = new ProcessBuilder(Main.APP).start();
                    var childrenCounter = ChildrenCounter.createOf(zooKeeper, Main.Z_NODE, zookeeperTree);
                    Thread thread = new Thread(childrenCounter);
                    thread.start();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        }else if(Event.EventType.NodeDeleted.equals(event.getType())){
            System.out.println(NODE_DELETED);
            process.destroy();
            System.exit(0);
        }

        try {
            zooKeeper.exists(Main.Z_NODE, this);
        } catch (KeeperException | InterruptedException e) {
            e.printStackTrace();
        }

    }

}
