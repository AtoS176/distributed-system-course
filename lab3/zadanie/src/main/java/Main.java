import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooKeeper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    final static String APP = "xfce4-terminal";
    final static String Z_NODE = "/z";
    final static String CONNECT_STRING = "127.0.0.1:2181";
    final static String Z_NODE_NOT_EXIST = "/z node doesn't exist";

    public static void main(String[] args) throws IOException, KeeperException, InterruptedException {
        final var zookeeper = new ZooKeeper(CONNECT_STRING, 5000, null);

        final var zookeeperTree = ZookeeperTree.createOf(zookeeper);
        final var watcher = ZookeeperWatcher.createOf(zookeeper, zookeeperTree);
        zookeeper.exists(Z_NODE, watcher);

        final var childrenCounter = ChildrenCounter.createOf(zookeeper, Z_NODE, zookeeperTree);
        Thread thread = new Thread(childrenCounter);
        thread.start();

        while(true){
            System.out.println("If you want to see all tree, then click ENTER");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            br.readLine();

            if(zookeeperTree.ifNodeExist(Z_NODE)){
                zookeeperTree.printChildren(Z_NODE);
            }else{
                System.out.println(Z_NODE_NOT_EXIST);
            }
        }

    }

}
