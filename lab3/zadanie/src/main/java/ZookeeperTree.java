import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

public class ZookeeperTree {
    final ZooKeeper zooKeeper;

    private ZookeeperTree(ZooKeeper zooKeeper) {
        this.zooKeeper = zooKeeper;
    }

    public static ZookeeperTree createOf(ZooKeeper zooKeeper){
        return new ZookeeperTree(zooKeeper);
    }

    public boolean ifNodeExist(String node){
        try {
            return zooKeeper.exists(node, false) != null;
        } catch (KeeperException | InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void subscribeChildren(String node, Watcher watcher) throws KeeperException, InterruptedException {
        for(String child : zooKeeper.getChildren(node, watcher)){
            subscribeChildren(node + "/" + child, watcher);
        }
    }

    public void printChildren(String node) throws KeeperException, InterruptedException {
        for(String child : zooKeeper.getChildren(node, false)){
            System.out.println(node + "/" + child);
            printChildren(node + "/" + child);
        }
    }

}
