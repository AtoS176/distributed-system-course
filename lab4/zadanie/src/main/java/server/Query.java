package server;

import client.Command;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Query implements Command {
    final int id;
    final int firstSatellite;
    final int lastSatellite;
    final int timeout;

    public Query(String query) {
        List<Integer> params = parseQuery(query);
        this.id = params.get(0);
        this.firstSatellite = params.get(1);
        this.lastSatellite = params.get(1) + params.get(2);
        this.timeout = params.get(3);
    }

    private List<Integer> parseQuery(String query){
        query = query.substring(1, query.length() - 1);
        query = query.replace(" ", "");
        String[] args = query.split(",");

        return Arrays.stream(args)
                .map(Integer::valueOf)
                .collect(Collectors.toList());
    }

    public int calculateRange(){
        return lastSatellite - firstSatellite;
    }

    public static String buildStringQuery(String id){
        Random random = new Random();
        int firstStation = 100 + random.nextInt(50);
        return "(" + id + ", " + firstStation + ", 50, 300)";
    }

}
