package server;

import redis.clients.jedis.JedisPoolConfig;

public class JedisConfig {
    public static JedisPoolConfig jedisPoolConfig(){
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(100);
        jedisPoolConfig.setMaxTotal(100);
        return jedisPoolConfig;
    }
}
