package server;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.*;
import client.*;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;


public class Dispatcher extends AbstractBehavior<Command> {
    private final JedisPool jedisPool = new JedisPool(JedisConfig.jedisPoolConfig());

    public Dispatcher(ActorContext<Command> context) {
        super(context);
    }

    public static Behavior<Command> create() {
        return Behaviors.setup(Dispatcher::new);
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(Request.class, this::onMessage)
                .onMessage(DbRequest.class, this::onDbRequest)
                .build();
    }

    private Behavior<Command> onMessage(Request request) {
        final long start = System.currentTimeMillis();
        final Query query = new Query(request.text);

        final ExecutorService executorService = Executors.newFixedThreadPool(query.calculateRange());
        final Map<Integer, SatelliteAPI.Status> statusMap = new ConcurrentHashMap<>();

        for (int id = query.firstSatellite; id < query.lastSatellite; id++) {
            final int finalId = id;

            Future<List<Object>> future = executorService.submit(() -> {
                SatelliteAPI.Status status = SatelliteAPI.getStatus(finalId);
                return List.of(finalId, status);
            });

            try {
                List<Object> result = future.get(query.timeout, TimeUnit.MILLISECONDS);
                statusMap.put((Integer) result.get(0), (SatelliteAPI.Status) result.get(1));
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
//                System.out.println("!! TIMEOUT !!");
            }

        }

        final double percent = (double) statusMap.size() / query.calculateRange();
        final Map<Integer, String> errorMap = new HashMap<>();

        statusMap.forEach((k, v) -> {
            if(!v.equals(SatelliteAPI.Status.OK)){
                errorMap.put(k, v.toString());
            }
        });

        final long time = System.currentTimeMillis() - start;
        final Response response = new Response(query.id, errorMap, percent, time);
        request.replyTo.tell(response);

        executorService.submit(() -> updateDb(errorMap));
        executorService.shutdown();
        return this;
    }

    private Behavior<Command> onDbRequest(DbRequest dbRequest) {
        Jedis jedis = jedisPool.getResource();
        Object number = jedis.hget("astraLink", String.valueOf(dbRequest.errorId));
        dbRequest.replyTo.tell(new DbResponse(dbRequest.errorId, Integer.parseInt(number.toString())));
        return this;
    }

    private void updateDb(Map<Integer, String> errorMap){
        Jedis jedis = jedisPool.getResource();
        Map<String, String> errors = jedis.hgetAll("astraLink");

        errorMap.forEach((k, v) -> {
            String key = String.valueOf(k);
            String value = errors.get(key);
            if(value != null){
                int number = Integer.parseInt(value) + 1;
                jedis.hset("astraLink", key, String.valueOf(number));
            }
        });
    }

}
