package client;

import akka.actor.typed.ActorRef;

public class Request implements Command{
    public final String text;
    public final ActorRef<Command> replyTo;
    public final long time;

    public Request(String text, ActorRef<Command> replyTo) {
        this.text = text;
        this.replyTo = replyTo;
        time = System.currentTimeMillis();
    }

}
