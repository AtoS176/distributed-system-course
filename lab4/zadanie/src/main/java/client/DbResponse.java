package client;

public class DbResponse implements Command {
    final int id;
    final int errors;

    public DbResponse(int id, int errors) {
        this.id = id;
        this.errors = errors;
    }

}
