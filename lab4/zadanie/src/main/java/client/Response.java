package client;

import java.util.Map;

public class Response implements Command {
    final int id;
    final Map<Integer, String> errors;
    final double successPercent;
    final long time;

    public Response(int id, Map<Integer, String> errors, double successPercent, long time) {
        this.id = id;
        this.errors = errors;
        this.successPercent = successPercent;
        this.time = time;
    }

}
