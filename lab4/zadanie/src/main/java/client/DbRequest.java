package client;

import akka.actor.typed.ActorRef;

public class DbRequest implements Command {
    public final int errorId;
    public final ActorRef<Command> replyTo;

    public DbRequest(int errorId, ActorRef<Command> replyTo) {
        this.errorId = errorId;
        this.replyTo = replyTo;
    }

}
