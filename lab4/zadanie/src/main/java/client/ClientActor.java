package client;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

public class ClientActor extends AbstractBehavior<Command> {
    private static ActorRef<Command> worker;

    public ClientActor(ActorContext<Command> context) {
        super(context);
    }

    public static Behavior<Command> create(ActorRef<Command> dispatcher) {
        worker = dispatcher;
        return Behaviors.setup(ClientActor::new);
    }

    @Override
    public Receive<Command> createReceive() {
        System.out.println("Create receive for Client");

        return newReceiveBuilder()
                .onMessage(Request.class, this::onRequest)
                .onMessage(Response.class, this::onResponse)
                .onMessage(DbRequest.class, this::onDbRequest)
                .onMessage(DbResponse.class, this::onDbResponse)
                .build();
    }

    private Behavior<Command> onRequest(Request msg) {
        worker.tell(msg);
        return this;
    }

    private Behavior<Command> onResponse(Response msg){
        System.out.println("### Response ###");
        System.out.println("ID : " + msg.id);
        System.out.println("Time : " + msg.time);
        System.out.println("Percent : " + msg.successPercent);
        System.out.println("Errors : {");
        msg.errors.forEach((k, v) -> System.out.println("   Key : " + k + " Value: " + v));
        System.out.println("}");
        return this;
    }

    private Behavior<Command> onDbRequest(DbRequest msg){
        worker.tell(msg);
        return this;
    }

    private Behavior<Command> onDbResponse(DbResponse dbResponse){
        if(dbResponse.errors != 0) {
            System.out.println("### Response ###");
            System.out.println("ID : " + dbResponse.id);
            System.out.println("ERROR Number : " + dbResponse.errors);
        }
        return this;
    }

}
