import akka.actor.typed.ActorRef;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.Behavior;
import akka.actor.typed.Terminated;
import akka.actor.typed.javadsl.Behaviors;
import client.ClientActor;
import client.Command;
import client.DbRequest;
import client.Request;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import redis.clients.jedis.Jedis;
import server.Dispatcher;
import server.Query;

import java.io.File;

public class Runner {
    public static Behavior<Void> create() {
        return Behaviors.setup(
                context -> {
                    ActorRef<Command> dispatcher = context.spawn(Dispatcher.create(), "server");

                    ActorRef<Command> first = context.spawn(ClientActor.create(dispatcher), "first");
                    ActorRef<Command> second = context.spawn(ClientActor.create(dispatcher), "second");
                    ActorRef<Command> third = context.spawn(ClientActor.create(dispatcher), "third");

                    Thread.sleep(2000);
                    first.tell(new Request(Query.buildStringQuery("1"), first));
                    first.tell(new Request(Query.buildStringQuery("2"), first));

                    second.tell(new Request(Query.buildStringQuery("3"), second));
                    second.tell(new Request(Query.buildStringQuery("4"), second));

                    third.tell(new Request(Query.buildStringQuery("5"), third));
                    third.tell(new Request(Query.buildStringQuery("6"), third));

                    Thread.sleep(2000);

                    for(int i = 100; i < 200; i++){
                        first.tell(new DbRequest(i, first));
                    }

                    return Behaviors.receive(Void.class)
                            .onSignal(Terminated.class, sig -> Behaviors.stopped())
                            .build();
                });
    }

    public static void main(String[] args) {
        initDb();
        File configFile = new File("src/main/resources/dispatcher.conf");
        Config config = ConfigFactory.parseFile(configFile);
        ActorSystem.create(Runner.create(), "AstraLinkSystem", config);
    }

    private static void initDb(){
        Jedis jedis = new Jedis();
        final String map = "astraLink";

        for(int i = 100; i < 200; i++){
            jedis.hset(map, String.valueOf(i), "0");
        }
    }

}
