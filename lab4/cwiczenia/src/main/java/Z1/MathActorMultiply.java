package Z1;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.util.concurrent.atomic.AtomicInteger;

public class MathActorMultiply extends AbstractBehavior<MathActor.MathCommandMultiply> {
    private final AtomicInteger operationCount = new AtomicInteger(0);

    // --- use messages from MathActor -> no need to define new ones

    // --- constructor and create
    public MathActorMultiply(ActorContext<MathActor.MathCommandMultiply> context) {
        super(context);
    }

    public static Behavior<MathActor.MathCommandMultiply> create() {
        return Behaviors.setup(MathActorMultiply::new);
    }

    // --- define message handlers
    @Override
    public Receive<MathActor.MathCommandMultiply> createReceive() {
        return newReceiveBuilder()
                .onMessage(MathActor.MathCommandMultiply.class, this::onMathCommandMultiply)
                .build();
    }

    private Behavior<MathActor.MathCommandMultiply> onMathCommandMultiply(MathActor.MathCommandMultiply mathCommandMultiply) {
        System.out.println("actorMultiply: operationCount:" + operationCount.incrementAndGet());
        int result = mathCommandMultiply.firstNumber * mathCommandMultiply.secondNumber;
        mathCommandMultiply.replyTo.tell(new MathActor.MathCommandResult(result));
        return this;
    }

}
