package Z2;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import akka.actor.typed.receptionist.Receptionist;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ActorTextService extends AbstractBehavior<ActorTextService.Command>  {

    // --- messages
    interface Command {}

    // TODO: new message type implementing Command, with Receptionist.Listing field -> Competed
    public static class ListeningResponse implements Command{
        final Receptionist.Listing listing;

        public ListeningResponse(Receptionist.Listing listing) {
            this.listing = listing;
        }
    }

    public static class Request implements Command {
        final String text;

        public Request(String text) {
            this.text = text;
        }
    }


    // --- constructor and create
    // TODO: field for message adapter -> Completed
    private List<ActorRef<String>> workers = new LinkedList<>();
    private ActorRef<Receptionist.Listing> listingAdapter;

    public ActorTextService(ActorContext<ActorTextService.Command> context) {
        super(context);

        // TODO: create message adapter -> Completed
        this.listingAdapter = context.messageAdapter(Receptionist.Listing.class, ListeningResponse::new);

        // TODO: subscribe to receptionist (with message adapter) -> Completed
        context.getSystem()
                .receptionist()
                .tell(Receptionist.subscribe(ActorUpperCase.upperCaseServiceKey, listingAdapter));
    }

    public static Behavior<Command> create() {
        return Behaviors.setup(ActorTextService::new);
    }

    // --- define message handlers
    @Override
    public Receive<Command> createReceive() {

        System.out.println("creating receive for text service");

        return newReceiveBuilder()
                .onMessage(Request.class, this::onRequest)
                // TODO: handle the new type of message -> Completed
                .onMessage(ListeningResponse.class, response -> onListeningResponse(response.listing))
                .build();
    }

    private Behavior<Command> onRequest(Request msg) {
        System.out.println("request: " + msg.text);
        for (ActorRef<String> worker : workers) {
            System.out.println("sending to worker: " + worker);
            worker.tell(msg.text);
        }
        return this;
    }

    // TODO: handle the new type of message -> Completed
    private Behavior<Command> onListeningResponse(Receptionist.Listing msg){
        workers = new ArrayList<>(msg.getAllServiceInstances(ActorUpperCase.upperCaseServiceKey));
        return this;
    }

}
