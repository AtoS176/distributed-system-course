package global;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class PublicMessageHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(PublicMessageHandler.class);
    private final String EXCHANGE_NAME = "message_channel";
    private final static String HOST = "localhost";

    public void handleMessage(String routingKey) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        // queue & bind
        String queueName = channel.queueDeclare().getQueue();
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        channel.queueBind(queueName, EXCHANGE_NAME, routingKey);

        //handle order
        Consumer consumer = handleOrder(channel);

        // start listening
        channel.basicConsume(queueName, true, consumer);
    }

    private Consumer handleOrder(Channel channel) {
        return new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, StandardCharsets.UTF_8);
                LOGGER.info("Received from ADMIN : " + message);
            }
        };
    }


}
