package global;

public enum MessageType {
    CLIENT("admin_client"),
    SHOP("admin_shop"),
    CLIENT_AND_SHOP("admin_shop.admin_client");

    public String routingKey;

    MessageType(String routingKey) {
        this.routingKey = routingKey;
    }

}
