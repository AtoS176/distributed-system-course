package client;

import admin.AdminReceiver;
import com.rabbitmq.client.*;
import global.PublicMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class OrderSender {
    private final static Logger LOGGER = LoggerFactory.getLogger(OrderSender.class);
    private final static String EXCHANGE_NAME = "shop";
    private final static String HOST = "localhost";
    private final static String PUBLIC_ROUTING_KEY = "#.admin_client.#";
    private final String name;

    public OrderSender(String name) {
        this.name = name;
    }

    public void run(){
        LOGGER.info("Order sender is already running.");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);

        try(Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()){

            //run answer handler from SHOP
            ApprovalHandler answerHandler = new ApprovalHandler(channel);
            AMQP.BasicProperties callbackProp = answerHandler.receive();

            //run message handler from ADMIN
            PublicMessageHandler publicMessageHandler = new PublicMessageHandler();
            publicMessageHandler.handleMessage(PUBLIC_ROUTING_KEY);

            while (true) {
                // read msg
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                final String routingKey = br.readLine();
                final String orderDetails = routingKey + "," + name;

                // break condition
                if ("exit".equals(routingKey)) {
                    break;
                }

                // publish to shop
                channel.basicPublish(EXCHANGE_NAME, routingKey, callbackProp, orderDetails.getBytes(StandardCharsets.UTF_8));

                //publish to admin
                channel.basicPublish(EXCHANGE_NAME, AdminReceiver.ROUTING_KEY, null, orderDetails.getBytes(StandardCharsets.UTF_8));

                System.out.println("Sent message: " + orderDetails);
            }

        }catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

}
