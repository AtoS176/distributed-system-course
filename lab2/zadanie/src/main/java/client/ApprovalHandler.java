package client;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ApprovalHandler {
    private final Logger LOGGER = LoggerFactory.getLogger(ApprovalHandler.class);
    private final Channel channel;

    public ApprovalHandler(Channel channel) {
        this.channel = channel;
    }

    public AMQP.BasicProperties receive() throws IOException {
        final Consumer consumer = handleAnswer(channel);
        final String callbackQueueName = channel.queueDeclare().getQueue();

        channel.basicConsume(callbackQueueName, true, consumer);

        return new AMQP.BasicProperties
                .Builder()
                .replyTo(callbackQueueName)
                .build();
    }

    private Consumer handleAnswer(Channel channel) {
        return new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body){
                String message = new String(body, StandardCharsets.UTF_8);
                LOGGER.info("Answer: " + message);
            }
        };
    }
}
