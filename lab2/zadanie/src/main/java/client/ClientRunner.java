package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientRunner {
    public static void main(String[] args) throws IOException {
        //read name
        System.out.println("Podaj nazwe ekipy : ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final String name = br.readLine();

        OrderSender orderProducer = new OrderSender(name);
        orderProducer.run();
    }
}
