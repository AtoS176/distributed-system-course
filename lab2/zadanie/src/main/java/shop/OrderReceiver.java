package shop;

import admin.AdminReceiver;
import com.rabbitmq.client.*;
import global.PublicMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class OrderReceiver {
    private final static Logger LOGGER = LoggerFactory.getLogger(OrderReceiver.class);
    private final static String EXCHANGE_NAME = "shop";
    private final static String HOST = "localhost";
    private final static String PUBLIC_ROUTING_KEY = "#.admin_shop.#";
    private final List<String> goods;
    private final String name;

    public OrderReceiver(List<String> goods, String name) {
        this.goods = goods;
        this.name = name;
    }

    public void run() throws IOException, TimeoutException {
        LOGGER.info("Consumer " + name + "is already running.");
        LOGGER.info("Routing keys : " + String.join(",", goods));

        //run message handler from ADMIN
        PublicMessageHandler publicMessageHandler = new PublicMessageHandler();
        publicMessageHandler.handleMessage(PUBLIC_ROUTING_KEY);

        for(String good : goods){
            runConnection(good, "queue_" + good);
        }
    }

    private void runConnection(String routingKey, String queueName) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        //activate QoS
        channel.basicQos(1);

        // queue & bind
        channel.queueDeclare(queueName, false, false, false, null);
        channel.queueBind(queueName, EXCHANGE_NAME, routingKey);
        LOGGER.info("Bind with queue : " + queueName);

        //handle order
        Consumer consumer = handleOrder(channel);

        // start listening
        LOGGER.info("Waiting for orders...");
        channel.basicConsume(queueName, true, consumer);

    }

    private Consumer handleOrder(Channel channel) {
        return new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, StandardCharsets.UTF_8);
                LOGGER.info("Receive message : " + message);

                final String[] names = message.split(",");
                final String orderName = names[0];
                final String clientName = names[1];

                final Order order = new Order(orderName, clientName);
                final String orderDetails = order.toString();
                LOGGER.info("Create :" + orderDetails);

                //send answer
                final String callbackQueue = properties.getReplyTo();
                final String answer = name + " zrealizował zamówienie " + orderDetails;
                System.out.println("Sent message : " + answer);

                //publish to client
                channel.basicPublish("", callbackQueue, null, answer.getBytes(StandardCharsets.UTF_8));

                //publish to admin
                channel.basicPublish(EXCHANGE_NAME, AdminReceiver.ROUTING_KEY, null, answer.getBytes(StandardCharsets.UTF_8));
            }
        };
    }

}
