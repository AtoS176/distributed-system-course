package shop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class ShopRunner {
    public static void main(String[] args) throws IOException, TimeoutException {

        //read name
        System.out.println("Podaj nazwe swojego sklepu : ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        final String name = br.readLine();

        //read goods
        System.out.println("Podaj typy przedmiotów, które chcesz sprzedać (podaj nazwy po przecinku) : ");
        br = new BufferedReader(new InputStreamReader(System.in));
        final String goods = br.readLine();

        List<String> goodsList = Arrays.asList(goods.split(","));
        OrderReceiver orderReceiver = new OrderReceiver(goodsList, name);
        orderReceiver.run();
    }
}
