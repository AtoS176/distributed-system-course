package shop;

public class Order {
    private final Long id;
    private final String name;
    private final String client;

    public Order(String name, String client) {
        this.id = OrderId.incrementAndGet();
        this.name = name;
        this.client = client;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", client='" + client + '\'' +
                '}';
    }
}
