package admin;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class AdminReceiver {
    private final static Logger LOGGER = LoggerFactory.getLogger(AdminReceiver.class);
    private final static String QUEUE_NAME = "admin_receiver";
    public final static String ROUTING_KEY = "admin_log";
    private final static String EXCHANGE_NAME = "shop";
    private final static String HOST = "localhost";

    public void receive() throws IOException, TimeoutException {
        LOGGER.info("Admin receiver is already running");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, ROUTING_KEY);
        LOGGER.info("Bind with queue : " + QUEUE_NAME);

        //handle order
        Consumer consumer = handleMessage(channel);

        // start listening
        LOGGER.info("Waiting for messages ...");
        channel.basicConsume(QUEUE_NAME, false, consumer);

    }

    private Consumer handleMessage(Channel channel) {
        return new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, StandardCharsets.UTF_8);
                LOGGER.info("Received message : " + message);
            }
        };
    }

}
