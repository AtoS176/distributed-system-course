package admin;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class AdminRunner {
    public static void main(String[] args) throws IOException, TimeoutException {
        AdminReceiver adminReceiver = new AdminReceiver();
        adminReceiver.receive();
        AdminSender adminSender = new AdminSender();
        adminSender.send();
    }
}
