package admin;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import global.MessageType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class AdminSender {
    private final static Logger LOGGER = LoggerFactory.getLogger(AdminReceiver.class);
    private final static String EXCHANGE_NAME = "message_channel";
    private final static String HOST = "localhost";

    public void send(){
        LOGGER.info("Admin sender is already running.");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(HOST);

        try(Connection connection = factory.newConnection();
            Channel channel = connection.createChannel()){

            while (true) {
                // read msg
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                final String message = br.readLine();
                final String routingKey = prepareRoutingKey(message.substring(0,2));

                // break condition
                if ("exit".equals(routingKey)) {
                    break;
                }

                final String messageWithoutCommand = message.substring(2);

                channel.basicPublish(EXCHANGE_NAME, routingKey, null, messageWithoutCommand.getBytes(StandardCharsets.UTF_8));
                LOGGER.info("Sent message: " + message);
            }

        }catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    private String prepareRoutingKey(String command){
        if(command.equals("-C")) return MessageType.CLIENT.routingKey;
        if(command.equals("-S")) return MessageType.SHOP.routingKey;
        if(command.equals("-A")) return MessageType.CLIENT_AND_SHOP.routingKey;
        return "exit";
    }
}
