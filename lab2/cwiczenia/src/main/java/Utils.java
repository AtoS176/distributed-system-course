import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import io.vavr.control.Try;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Utils {
    public static String choseExchangeType(Channel channel, BuiltinExchangeType type) {
        final String fanoutName = "exchange1";
        final String directName = "exchange2";
        final String topicName = "exchange3";

        if(BuiltinExchangeType.TOPIC.equals(type)){
            Try.run(() -> channel.exchangeDeclare(topicName, type));
            return topicName;
        }else if(BuiltinExchangeType.DIRECT.equals(type)){
            Try.run(() -> channel.exchangeDeclare(directName, type));
            return directName;
        }

        //fanout is default
        Try.run(() -> channel.exchangeDeclare(fanoutName, type));
        return fanoutName;
    }

    public static String readKey() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter key: ");

        String key;
        while ((key = br.readLine()) == null);

        System.out.println("Your key is : " + key);
        return key;
    }
}
