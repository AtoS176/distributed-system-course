<h1>Słów kilka o RabbitMQ</h1>
<p>Jest to broker wiadomości o otwartym kodzie zródłowym. Został napisany w Erlangu, a jego API jest dostępne dla wielu języków programowania.</p>
<p>W aplikacjach, które korzystają z RabbitMQ możemy wyodrębnić trzy podstawowe elementy : </p>
<ul>
<li>Producent - tworzy zamówienie i wysyła je do kolejki</li>
<li>Kolejka - otrzymuje zamówienie od prodcuenta i przechowuje je</li>
<li>Konsument - przetwarza zamówienia z kolejki</li>
</ul>
<br>
<p>Do komunikacji z RabbitMQ wykorzystuje się protokół AMQP. Ramka wiadomości
w tym protokole składa się z : </p>
<ul>
<li>ładunku - to treść naszej wiadomości i w żaden sposób nie jest modyfikowana przez system kolejkowania</li>
<li>atrybuty - dodatkowe elementy, które pozwalają na zidentyfikowanie odbiorców oraz sposobu ich dostarczenia, są ustawiane w momencie publikacji wiadomości.
<ul>
<li>content type - typ danych</li>
<li>content encoding - kodowanie danych</li>
<li>routing key - klucz routingu</li>
<li>delivery mode - sposób dostarczenia wiadomości</li>
<li>message priority - priorytet</li>
<li>message publishing timestamp - czas publikacji wiadomości</li>
<li>expiration period - opóznienie w dostarczeniu wiadomości [ms]</li>
<li>published application id - identyfikator producenta</li>
</ul>
</li>
</ul>

<p>W praktyce zamówienia najpierw trafiają do <b>centrali wiadomości</b>, a dopiero pózniej są przekazywane do odpowiedniej kolejki. Mechanizm przypisywania zamówień do określonej kolejki nazywany jest 'binding'. Najłatwiejszym sposobem stworzenia nowej centrali wiadomości jest wykonanie poniższej komendy : <b>sudo rabbitmqadmin declare exchange name="nazwa_centrali" type=direct</b> .</p>

<p>Jest kilka typów centrali, które mają wpływ na wykorzystanie parametru 'routing key'.</p>
<ul>
<li>direct - centrala wiadomości bezpośrednich, aby wiadomość została przydzielona do konkretnej kolejki, musi posiadać odpowiednią wartość parametru routing key, zgodną z wartością parametru binding key</li>
<li>topic - centrala wiadomości tematycznych, działa podobnie jak direct exchange, jednak parametr binding key nie jest już konkretną wartościa, a wyrażeniem składającym się ze słów rozdzielonych kropkami oraz #.</li>
<li>headers - centrala wiadomości z nagłowkami, routing key jest ignorowany a wiadomość jest przesyłana na podstawie nagłówka. Typy dopasować : any oraz all</li>
<li>fanout - centrala rozgłośni wiadomości, żaden parametr nie jest brany pod uwagę - wiadomości trafiają do wszystkich aktywnych kolejek.</li>
</ul>
